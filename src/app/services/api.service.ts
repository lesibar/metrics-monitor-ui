import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment";
import { Breach} from "../model/breach";
import { QueueManager} from "../model/queue.manager";
import { Queue} from "../model/queue";
import { Threshold } from "../model/threshold";
import {HttpParams} from "@angular/common/http";
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class ApiService {

    BACKEND_URL: string = environment.serverUrl;
    constructor(private httpClient: HttpClient) { }


    getQueueManagers() : Promise<QueueManager[]>{
        const URL: string = this.BACKEND_URL + '/hosts';
        return this.httpClient
            .get<QueueManager[]>(URL)
            .toPromise();
    }

    getQueueManager(host: string, queueManager: string) {
        const URL: string = this.BACKEND_URL + '/hosts/' + host + '/queue-managers/' + queueManager;
        return this.httpClient
            .get(URL)
            .toPromise();
    }

    getQueueManagerWithTopics(host: string, queueManager: string) {
        const URL: string = this.BACKEND_URL + '/hosts/' + host + '/queue-managers/' + queueManager + '/topics';
        return this.httpClient
            .get(URL)
            .toPromise();
    }

    getQueueManagerWithQueues(host: string, queueManager: string) : Promise<QueueManager> {
        const URL: string = this.BACKEND_URL + '/hosts/' + host + '/queue-managers/' + queueManager + '/queues';
        return this.httpClient
            .get<QueueManager>(URL)
            .toPromise();
    }

    getQueue(host: string, queueManager: string, queueName) {
        const URL: string = this.BACKEND_URL + '/hosts/' + host + '/queue-managers/' + queueManager + '/queues/' + queueName;
        return this.httpClient
            .get(URL)
            .toPromise();
    }
   fetchBreaches(host: string, queueManager: string) : Promise<Breach[]>  {
        const URL: string = this.BACKEND_URL + '/breaches/';
        const params = new HttpParams().set('host', host).set('queueManager', queueManager);
        return this.httpClient
            .get<Breach[]>(URL, {params})
            .toPromise();
    }


    addThreshold(host: string, queueManager: string, queueName:string, threshold : Threshold): Promise<Queue>{
        const URL: string = this.BACKEND_URL + '/hosts/' + host + '/queue-managers/' + queueManager + '/queues/' + queueName+'/thresholds/';
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json'
            })
          };
        return this.httpClient
            .post<Queue>(URL, threshold, httpOptions)
            .toPromise();
    }
}
