import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { QueueMangersComponent } from '../../queue-managers/queue-managers.component';
import { QueuesComponent } from '../../queues/queues.component';
import { QueueComponent } from '../../queue/queue.component';


export const AdminLayoutRoutes: Routes = [

    { path: 'dashboard',      component: DashboardComponent },
    { path: 'queue-managers', component: QueueMangersComponent },
    { path: 'queues',  component: QueuesComponent},
    { path: 'queue',  component: QueueComponent}
];
