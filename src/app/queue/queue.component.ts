import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Queue } from '../model/queue';
import { Data } from '../model/data';
import { ActivatedRoute } from '@angular/router';
import { Threshold } from '../model/threshold';


@Component({
    selector: 'app-queue',
    templateUrl: './queue.component.html',
    styleUrls: ['./queue.component.scss']
})
export class QueueComponent implements OnInit {

    queue: Queue;
    threshold: Threshold = new Threshold();

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private api: ApiService, private data: Data) { }

    ngOnInit() {
        this.queue = this.data.storage["queue"];
        console.log('Properties are ' + this.queue.properties);

    }
    generateArray(obj) {
        return Object.keys(obj).map((key) => { return { key: key, value: obj[key] } });
    }

    public onSubmit() {
        this.api.addThreshold(this.queue.queueManager.hostName, this.queue.queueManager.queueManagerName, this.queue.name, this.threshold)
        .then((data) => {
            let qm = this.queue.queueManager;
            this.queue = data;
            this.queue.queueManager = qm;
            console.log('threshold is ' + JSON.stringify(this.queue.thresholds));
        }).catch(error => {
            console.log('Error occurred : ', error);
        });
        ;

    }

}
