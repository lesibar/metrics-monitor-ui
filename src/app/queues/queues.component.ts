import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { QueueManager } from '../model/queue.manager';
import { Data } from '../model/data';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-queues',
    templateUrl: './queues.component.html',
    styleUrls: ['./queues.component.scss']
})
export class QueuesComponent implements OnInit {

    queueManager: QueueManager;

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private api: ApiService, private data: Data) { }

    ngOnInit() {
        this.queueManager = this.data.storage["queueManager"];
        this.api.getQueueManagerWithQueues(this.data.storage["hostName"], this.data.storage["queueManagerName"]).then((serverData) => {
            this.queueManager = serverData;
            //this.queueManager = new QueueManager(serverData["queueManagerName"], serverData["queueManagerName"], serverData['queues'], null, serverData['properties']);
            this.data.storage = { "queueManager": this.queueManager };
            //this.getQueueManagerWithTopics();

        }).catch(error => {
            console.log('Poop occurred. ', error);
        });
    }


    openQueue(queueName: string) {
        this.queueManager.queues.forEach(q => {
            if (q.name == queueName) {
                q.queueManager = this.queueManager;
                this.data.storage["queue"] = q;
            }
        });
        this.router.navigate(["/queue"]);
    }

}
