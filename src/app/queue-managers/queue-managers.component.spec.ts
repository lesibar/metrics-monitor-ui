import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {QueueMangersComponent} from './queue-mangers.component';


describe('TableListComponent', () => {
  let component: QueueMangersComponent;
  let fixture: ComponentFixture<QueueMangersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueueMangersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueMangersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
