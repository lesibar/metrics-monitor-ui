import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { QueueManager } from '../model/queue.manager';
import { Router } from "@angular/router";
import { Data } from '../model/data';


@Component({
    selector: 'app-table-list',
    templateUrl: './queue-managers.component.html',
    styleUrls: ['./queue-managers.component.css']
})
export class QueueMangersComponent implements OnInit {

    queueManagers: Array<QueueManager> = [];

    constructor(private api: ApiService, private router: Router, private dataStore: Data) { }

    ngOnInit() {
        this.getQueueManagers();
    }

    getQueueManagers() {
        this.api.getQueueManagers()
            .then((data) => {
                this.queueManagers = data;
                this.dataStore.storage["queueManagers"] = this.queueManagers;
            }).catch(error => {
                console.log('Poop occurred : ', error);
            });
    }

    openQueueManager(hostName: string, queueManagerName: string) {
        this.dataStore.storage["hostName"] = hostName;
        this.dataStore.storage["queueManagerName"] = queueManagerName;
        this.queueManagers.forEach(qm => {
            if (qm.queueManagerName == queueManagerName) {
                this.dataStore.storage["queueManager"] = qm;
            }
        });
        this.router.navigate(["/queues"]);
    }
}
