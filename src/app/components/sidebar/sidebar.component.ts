import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Data } from '../../model/data';
import { Breach } from '../../model/breach';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/queue-managers', title: 'Queue Managers', icon: 'content_paste', class: '' },
    // { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];
    breaches: Array<Breach> = [];
    breachName:string = 'Breaches';

    constructor(private apiService: ApiService, private data: Data) {

    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        let host = '';
        if (this.data.storage["hostName"] != undefined) {
            host = this.data.storage["hostName"];
        }

        let queueManagerName = '';
        if (this.data.storage["queueManagerName"] != undefined) {
            queueManagerName = this.data.storage["queueManagerName"];
            this.breachName = queueManagerName + ' Breaches';
        }
        this.apiService.fetchBreaches(host, queueManagerName)
            .then((serverData) => {
                this.breaches = serverData;
            }).catch(error => {
                console.log('Poop occurred. ', error);
            });

    }
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
