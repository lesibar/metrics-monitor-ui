import { QueueManager } from './queue.manager';
export class Threshold {

    uuid: string;
    name: string;
    destinationName: string;
    mqPropertyName: string;
    alertType: string;
    alertDetails: string;
    queueManager: QueueManager;
    condition: string;
    timeUnit: string;
    value: number;
}