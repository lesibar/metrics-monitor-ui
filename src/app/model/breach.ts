import { Threshold } from './threshold';
export class Breach {

    threshold: Threshold;
    actualValue: number;
    status: string;
    alertCount:number;

}
