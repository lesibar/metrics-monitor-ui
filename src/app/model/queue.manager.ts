import {Queue} from './queue';
import {Topic} from './topic';

export class QueueManager {
    queueManagerName: string;
    hostName: string;
    queues: Array<any> = [];
    topics: Array<Topic> = [];
    properties: Array<any> = [];

    constructor(queueManagerName: string, hostName: string, queues: Array<any>, topics: Array<any>, properties: Array<any>) {
        this.queueManagerName = queueManagerName;
        this.hostName = hostName;
        this.queues = queues;
        this.topics = topics;
        this.properties = properties;
    }
}
