import { Threshold } from './threshold';
import { QueueManager } from './queue.manager';

export class Queue {
    name: string;
    queueDepth: number;
    properties: any;
    thresholds: Array<Threshold> = [];
    queueManager: QueueManager;

    constructor(name: string, queueDepth: number){
        this.name = name;
        this.queueDepth = queueDepth;
    }
    
}
